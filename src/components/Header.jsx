import React, { Component } from 'react';

function Header() {
  return(
    <header className="App-header">
      <div className="container">
        <h1 className="App-title">React Dogs</h1>
      </div>
    </header>
  )
}

export default Header;