import React, { Component } from 'react';
import './css/App.css';

import Header from './components/Header';

class App extends Component {
  state = {
    dog_breeds: [],
    fetching: true
  }
  render() {
    return (
      <div className="App">
        <Header/>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;
